<?php

use Illuminate\Http\Request;

Route::group([
    'prefix' => 'reviews',
    'middleware' => 'auth:api'
], function () {
    Route::get('', 'ReviewController@getAll');
    Route::get('{id}', 'ReviewController@getById');
    Route::post('', 'ReviewController@create');
    Route::put('{id}', 'ReviewController@update');
    Route::delete('{id}', 'ReviewController@delete');
});

Route::group([
    'prefix' => 'salons'
], function () {
    Route::get('', 'SalonController@getAll');
    Route::get('{id}', 'SalonController@getById');
    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('{id}/reviews', 'SalonController@getReviews');
        Route::get('{id}/reservations', 'SalonController@getReservations');
        Route::post('', 'SalonController@create');
        Route::put('{id}', 'SalonController@update');
        Route::delete('{id}', 'SalonController@delete');
    });
});

Route::group([
    'prefix' => 'employees',
    'middleware' => 'auth:api'
], function () {
    Route::get('', 'EmployeeController@getAll');
    Route::get('{id}', 'EmployeeController@getById');
    Route::get('{id}/reservations', 'EmployeeController@getReservations');
    Route::post('', 'EmployeeController@create');
    Route::put('{id}', 'EmployeeController@update');
    Route::delete('{id}', 'EmployeeController@delete');
});

Route::group([
    'prefix' => 'services',
    'middleware' => 'auth:api'
], function () {
    Route::get('', 'ServiceController@getAll');
    Route::get('{id}', 'ServiceController@getById');
    Route::post('', 'ServiceController@create');
    Route::put('{id}', 'ServiceController@update');
    Route::delete('{id}', 'ServiceController@delete');
});

Route::group([
    'prefix' => 'reservations',
    'middleware' => 'auth:api'
], function () {
    Route::get('', 'ReservationController@getAll');
    Route::get('{id}', 'ReservationController@getById');
    Route::post('', 'ReservationController@create');
    Route::put('{id}', 'ReservationController@update');
    Route::delete('{id}', 'ReservationController@delete');
});

Route::group([
    'prefix' => 'users'
], function () {
    Route::post('', 'UserController@create');
    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('', 'UserController@getAll');
        Route::get('{id}', 'UserController@getById');
        Route::get('{id}/reviews', 'UserController@getReviews');
        Route::get('{id}/reservations', 'UserController@getReservations');
        Route::put('{id}', 'UserController@update');
        Route::delete('{id}', 'UserController@delete');
    });
});

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
    });
});