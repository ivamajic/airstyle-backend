<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalonsServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salons_services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('salonId');
            $table->unsignedBigInteger('serviceId');
            $table->foreign('salonId')->references('id')->on('salons');
            $table->foreign('serviceId')->references('id')->on('services');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salons_services');
    }
}
