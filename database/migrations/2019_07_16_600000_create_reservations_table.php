<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->dateTimeTz('startTime');
            $table->dateTimeTz('endTime');
            $table->string('notes')->nullable();
            $table->unsignedBigInteger('userId');
            $table->unsignedBigInteger('salonId');
            $table->unsignedBigInteger('employeeId');
            $table->foreign('userId')->references('id')->on('users');
            $table->foreign('salonId')->references('id')->on('salons');
            $table->foreign('employeeId')->references('id')->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
