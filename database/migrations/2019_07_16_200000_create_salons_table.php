<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('name');
            $table->string('image')->nullable();
            $table->float('workHoursStart', 4, 2);
            $table->float('workHoursEnd', 4, 2);
            $table->string('email')->nullable();
            $table->string('address');
            $table->string('city');
            $table->string('country');
            $table->string('postCode', 5);
            $table->float('latitude');
            $table->float('longitude');
            $table->string('phone');
            $table->float('rating');
            $table->integer('noRatings');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salons');
    }
}
