<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firstName', 20);
            $table->string('lastName', 20);
            $table->string('image')->nullable();
            $table->enum('role', ['user', 'salon_admin', 'sys_admin'])->default('user');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->text('accessToken')->nullable();
            $table->timestamp('tokenExpiresAt')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->unsignedBigInteger('salonId')->nullable();
            $table->foreign('salonId')->references('id')->on('salons');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
