<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = [
        'startTime',
        'endTime',
        'notes',
        'userId',
        'salonId',
        'employeeId'
    ];

    /** Returns the salon which the reservation was made for. */
    public function salon() {
        return $this->belongsTo('App\Salon', 'salonId');
    }

    /** Returns the user who made the reservation. */
    public function user() {
        return $this->belongsTo('App\User', 'userId');
    }

    /** Returns the employee the reservation was made for. */
    public function employee() {
        return $this->belongsTo('App\Employee', 'employeeId');
    }

    /** Returns the services the reservation was made for. */
    public function services() {
        return $this->belongsToMany('App\Service', 'reservations_services', 'reservationId', 'serviceId');
    }
}
