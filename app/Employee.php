<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
        'firstName',
        'lastName',
        'image',
        'rating',
        'noRatings',
        'salonId'
    ];

    /** Returns the salon which the employee works in. */
    public function salon() {
        return $this->belongsTo('App\Salon', 'salonId');
    }

    /** Returns the reservations for the employee. */
    public function reservations() {
        return $this->hasMany('App\Reservation', 'employeeId');
    }
}
