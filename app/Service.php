<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'name',
        'price',
        'duration'
    ];

    /** Returns all the salons that offer the service. */
    public function salons() {
        return $this->hasMany('App\Salon', 'salonId');
    }

    /** Returns all the reservations that contain the service. */
    public function reservations() {
        return $this->hasMany('App\Reservation', 'serviceId');
    }
}
