<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = [
        'title',
        'description',
        'rating',
        'salonId',
        'userId'
    ];

    /** Returns the salon which the review was written for. */
    public function salon() {
        return $this->belongsTo('App\Salon', 'salonId');
    }

    /** Returns the user who wrote the review. */
    public function user() {
        return $this->belongsTo('App\User', 'userId');
    }
}
