<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salon extends Model
{
    protected $fillable = [
        'name',
        'image',
        'workHoursStart',
        'workHoursEnd',
        'email',
        'address',
        'city',
        'country',
        'postCode',
        'latitude',
        'longitude',
        'phone',
        'rating',
        'noRatings'
    ];

    /** Returns all the reviews of the salon. */
    public function reviews() {
        return $this->hasMany('App\Review', 'salonId');
    }

    /** Returns all the salon's employees. */
    public function employees() {
        return $this->hasMany('App\Employee', 'salonId');
    }

    /** Returns all the salon's app admins. */
    public function admins() {
        return $this->hasMany('App\User', 'salonId');
    }

    /** Returns all the reservations for the salon. */
    public function reservations() {
        return $this->hasMany('App\Reservation', 'salonId');
    }

    /** Returns all the services salon offers. */
    public function services() {
        return $this->belongsToMany('App\Service', 'salons_services', 'salonId', 'serviceId');
    }
}
