<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalonService extends Model
{
    protected $fillable = [
        'salonId',
        'serviceId'
    ];

    protected $table = 'salons_services';
}
