<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Http\Resources\User as UserResource;

class AuthController extends Controller
{
    public function login(Request $request) {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        
        $credentials = request(['email', 'password']);
        
        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
            
        $user = $request->user();
        
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        
        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1)->setTimezone('UTC');
        } else {
            $token->expires_at = Carbon::now()->addHours(1)->setTimezone('UTC');
        }

            if ($token->save()) {
                $user->accessToken = $tokenResult->accessToken;
                $user->tokenExpiresAt = Carbon::parse($tokenResult->token->expires_at)->toDateTimeString();
                if ($user->save()) {
                    return $user;
                }
            }
        }

    public function logout(Request $request)
    {
        $user = $request->user();
        $user->accessToken = null;
        $user->tokenExpiresAt = null;
        if ($request->user()->token()->revoke() && $user->save()) {
            return response()->json([
                'message' => 'Successfully logged out'
            ]);
        }
        return response()->json([
            'error' => 'Not logged out'
        ]);
    }
}
