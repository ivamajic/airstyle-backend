<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

use App\User;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\UserCollection;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\ReviewCollection;
use App\Http\Resources\ReservationCollection;

class UserController extends Controller
{
    public function getAll() {
        return User::orderBy('lastName', 'desc')->get();
    }

    public function getById($id) {
        return User::findOrFail($id);
    }

    public function getReviews($userId) {
        $user = User::findOrFail($userId);
        return $user->reviews()->get();
    }

    public function getReservations($userId) {
        $user = User::findOrFail($userId);
        $reservations = $user->reservations()->get();        
        foreach($reservations as $reservation) {
            $reservation->user = $reservation->user()->get();
            $reservation->salon = $reservation->salon()->get();
            $reservation->employee = $reservation->employee()->get();
            $reservation->services = $reservation->services()->get();
        }
        return $reservations;
    }

    public function create(Request $request) {
        $user = new User;
        $user->firstName = $request->input('firstName');
        $user->lastName = $request->input('lastName');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->image = $request->input('image');
        $user->role = 'user';
        $user->salonId = $request->input('salonId');
        if($user->save()) {                    
            $credentials = request(['email', 'password']);
            
            if(!Auth::attempt($credentials))
                return response()->json([
                    'message' => 'Unauthorized'
                ], 401);
                
            $user = $request->user();
            
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            
            if ($request->remember_me) {
                $token->expires_at = Carbon::now()->addWeeks(1)->setTimezone('UTC');
            } else {
                $token->expires_at = Carbon::now()->addHours(1)->setTimezone('UTC');
            }

            if ($token->save()) {
                $user->accessToken = $tokenResult->accessToken;
                $user->tokenExpiresAt = Carbon::parse($tokenResult->token->expires_at)->toDateTimeString();
                if ($user->save()) {
                    return $user;
                }
            }
        }
    }

    public function update(Request $request, $id) {
        $user = User::findOrFail($id);
        if ($request->input('firstName')) {
            $user->firstName = $request->input('firstName');
        }
        if ($request->input('lastName')) {
            $user->lastName = $request->input('lastName');
        }
        if ($request->input('image')) {
            $user->image = $request->input('image');
        }
        if ($request->input('role')) {
            $user->role = $request->input('role');
        }
        if ($request->input('password')) {
            $user->password = Hash::make($request->input('password'));
        }
        if ($request->input('salonId')) {
            $user->salonId = $request->input('salonId');
            if ($user->role !== "sys_admin") {
                $user->role = "salon_admin";
            }
        }
        if($user->save()) {
            return $user;
        }
    }

    public function delete($id) {
        $user = User::findOrFail($id);
        if ($user->salon === null) {
            if ($user->delete()) {
                return "Successfully deleted.";
            }
        } else {
            return "Can't delete admin";
        }
    }
}
