<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Employee;
use App\Http\Resources\Employee as EmployeeResource;
use App\Http\Resources\EmployeeCollection;
use App\Http\Resources\ReservationCollection;
use DB;

class EmployeeController extends Controller
{
    public function getAll() {
        return Employee::orderBy('lastName', 'desc')->get();
    }

    public function getById($id) {
        return Employee::findOrFail($id);
    }

    public function getReservations($employeeId) {
        $employee = Employee::findOrFail($employeeId);
        return $employee->reservations()->get();
    }

    public function create(Request $request) {
        $employee = new Employee;
        $employee->firstName = $request->input('firstName');
        $employee->lastName = $request->input('lastName');
        $employee->image = $request->input('image');
        $employee->rating = 0;
        $employee->noRatings = 0;
        $employee->salonId = $request->input('salonId');
        if($employee->save()) {
            return $employee;
        }
    }

    public function update(Request $request, $id) {
        $employee = Employee::findOrFail($id);
        if ($request->input('firstName')) {
            $employee->firstName = $request->input('firstName');
        }
        if ($request->input('lastName')) {
            $employee->lastName = $request->input('lastName');            
        }
        if ($request->input('image')) {
            $employee->image = $request->input('image');            
        }
        if ($request->input('rating') && $request->input('noRatings')) {
            $employee->rating = $request->input('rating');
            $employee->noRatings = $request->input('noRatings');            
        }
        if ($employee->save()) {
            return $employee;
        }
    }

    public function delete($id) {
        $employee = Employee::findOrFail($id);
        foreach($employee->reservations as $reservation) {
            DB::table('reservations_services')->where('reservationId','=',$reservation->id)->delete();
        }
        $employee->reservations()->delete();
        if ($employee->delete()) {
            return "Successfully deleted.";
        }
    }
}
