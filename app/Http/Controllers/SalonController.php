<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Salon;
use App\Http\Resources\Salon as SalonResource;
use App\Http\Resources\SalonCollection;
use App\Http\Resources\ReviewCollection;
use App\Http\Resources\ReservationCollection;
use DB;

class SalonController extends Controller
{
    public function getAll() {
        $salons = Salon::orderBy('created_at', 'desc')->get();
        foreach($salons as $salon) {
            $salon->services = $salon->services()->get();
            $salon->employees = $salon->employees()->get();
        }
        return $salons;
    }

    public function getById($id) {
        $salon = Salon::findOrFail($id);
        $salon->services = $salon->services()->get();
        $salon->employees = $salon->employees()->get();
        return $salon;
    }

    public function getReviews($salonId) {
        $salon = Salon::findOrFail($salonId);
        $reviews = $salon->reviews()->get();
        foreach($reviews as $review) {
            $review->user = $review->user;
            $review->salon = $review->salon;
        }
        return $reviews;
    }

    public function getReservations($salonId) {
        $salon = Salon::findOrFail($salonId);
        $reservations = $salon->reservations()->get();        
        foreach($reservations as $reservation) {
            $reservation->user = $reservation->user()->get();
            $reservation->salon = $reservation->salon()->get();
            $reservation->employee = $reservation->employee()->get();
            $reservation->services = $reservation->services()->get();
        }
        return $reservations;
    }

    public function create(Request $request) {
        $salon = new Salon;
        $salon->name = $request->input('name');
        $salon->image = $request->input('image');
        $salon->workHoursStart = $request->input('workHoursStart');
        $salon->workHoursEnd = $request->input('workHoursEnd');
        $salon->email = $request->input('email');
        $salon->address = $request->input('address');
        $salon->city = $request->input('city');
        $salon->country = $request->input('country');
        $salon->postCode = $request->input('postCode');
        $salon->latitude = $request->input('latitude');
        $salon->longitude = $request->input('longitude');
        $salon->phone = $request->input('phone');
        $salon->rating = 0;
        $salon->noRatings = 0;
        if ($salon->save()) {
            return $salon;
        }
    }

    public function update(Request $request, $id) {
        $salon = Salon::findOrFail($id);
        if($request->input('name')) {
            $salon->name = $request->input('name');
        }
        if($request->input('image')) {
            $salon->image = $request->input('image');
        }
        if($request->input('workHoursStart')) {
            $salon->workHoursStart = $request->input('workHoursStart');
        }
        if($request->input('workHoursEnd')) {
            $salon->workHoursEnd = $request->input('workHoursEnd');
        }
        if($request->input('email')) {
            $salon->email = $request->input('email');
        }
        if($request->input('address') && $request->input('city') && $request->input('postCode') && $request->input('latitude') && $request->input('longitude')&& $request->input('country')) {            
            $salon->address = $request->input('address');
            $salon->city = $request->input('city');
            $salon->postCode = $request->input('postCode');
            $salon->latitude = $request->input('latitude');
            $salon->longitude = $request->input('longitude');
            $salon->country = $request->input('country');
        }
        if($request->input('phone')) {
            $salon->phone = $request->input('phone');
        }
        if ($request->input('rating') && $request->input('noRatings')) {
            $salon->rating = $request->input('rating');
            $salon->noRatings = $request->input('noRatings');
        }
        if ($salon->save()) {
            return $salon;
        }
    }

    public function delete($id) {
        $salon = Salon::findOrFail($id);
        DB::table('salons_services')->where(['salonId' => $id])->delete();
        $salon->reviews()->delete();
        foreach($salon->reservations as $reservation) {
            DB::table('reservations_services')->where(['reservationId' => $reservation->id])->delete();
        }
        $salon->reservations()->delete();
        $salon->employees()->delete();
        foreach($salon->admins as $admin) {
            $user = User::findOrFail($admin->id);
            if ($user->role !== "sys_admin") {
                $user->role = "user";
            }
        }
        if ($salon->delete()) {
            return "Successfully deleted.";
        }
    }
}
