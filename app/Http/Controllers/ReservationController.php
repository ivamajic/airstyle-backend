<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use App\Reservation;
use App\ReservationService;
use App\Http\Resources\Reservation as ReservationResource;
use App\Http\Resources\ReservationCollection;

class ReservationController extends Controller
{
    public function getAll() {
        $reservations = Reservation::orderBy('created_at', 'desc')->get();
        foreach($reservations as $reservation) {
            $reservation->user = $reservation->user()->get();
            $reservation->salon = $reservation->salon()->get();
            $reservation->employee = $reservation->employee()->get();
            $reservation->services = $reservation->services()->get();
        }
        return $reservations;
    }

    public function getById($id) {
        $reservation = Reservation::findOrFail($id);
        $reservation->user = $reservation->user()->get();
        $reservation->salon = $reservation->salon()->get();
        $reservation->employee = $reservation->employee()->get();
        return $reservation;
    }

    public function create(Request $request) {
        $reservation = new Reservation;
        $reservation->startTime = $request->input('startTime');
        $reservation->endTime = $request->input('endTime');
        $reservation->notes = $request->input('notes');
        $reservation->userId = $request->input('userId');
        $reservation->salonId = $request->input('salonId');
        $reservation->employeeId = $request->input('employeeId');
        if($reservation->save()) {
            foreach($request->services as $service) {
                $reservation_service = new ReservationService;
                $reservation_service->reservationId = $reservation->id;
                $reservation_service->serviceId = $service;
                $reservation_service->save();
            }
            return $reservation;
        }    
    }

    public function update(Request $request, $id) {
        $reservation = Reservation::findOrFail($id);
        if ($request->input('startTime')) {
            $reservation->startTime = $request->input('startTime');
        }
        if ($request->input('endTime')) {
            $reservation->endTime = $request->input('endTime');
        }
        if ($request->input('notes')) {
            $reservation->notes = $request->input('notes');
        }
        if ($request->input('employeeId')) {
            $reservation->employeeId = $request->input('employeeId');
        }
        if($reservation->save()) {
            if ($request->services) {
                DB::table('reservations_services')->where('reservationId','=',$id)->delete();
                foreach($request->services as $service) {
                    $reservation_service = new ReservationService;
                    $reservation_service->reservationId = $reservation->id;
                    $reservation_service->serviceId = $service;
                    $reservation_service->save();
                }
            }
            return $reservation;
        }   
    }

    public function delete($id) {
        $reservation = Reservation::findOrFail($id);
        DB::table('reservations_services')->where('reservationId','=',$id)->delete();
        if ($reservation->delete()) {
            return "Successfully deleted.";
        }
    }
}
