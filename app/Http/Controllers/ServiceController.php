<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Service;
use App\SalonService;
use App\Http\Resources\Service as ServiceResource;
use App\Http\Resources\ServiceCollection;
use DB;

class ServiceController extends Controller
{
    public function getAll() {
        return Service::orderBy('created_at', 'desc')->get();
    }

    public function getById($id) {
        return Service::findOrFail($id);
    }

    public function create(Request $request) {
        if ($request->input('id')) {
            $service = Service::findOrFail($request->input('id'));
            $salon_service = new SalonService;
            $salon_service->salonId = $request->input('salonId');
            $salon_service->serviceId = $request->input('id');
            if ($salon_service->save()) {
                return $service;
            }
        }
        else if ($request->input('name') && $request->input('price') && $request->input('duration')) {
            $service = new Service;
            $service->name = $request->input('name');
            $service->price = $request->input('price');
            $service->duration = $request->input('duration');
            if($service->save()) { 
                $salon_service = new SalonService;
                $salon_service->salonId = $request->input('salonId');
                $salon_service->serviceId = $service->id;
                if ($salon_service->save()) {
                    return $service;
                }
            }
        } 
    }

    public function update(Request $request, $id) {
        $service = Service::findOrFail($id);
        if ($request->input('name')) {
            $service->name = $request->input('name');
        }
        if ($request->input('price')) {
            $service->price = $request->input('price');
        }
        if ($request->input('duration')) {
            $service->duration = $request->input('duration');
        }
        if($service->save()) {
            return $service;
        }
    }

    public function delete(Request $request, $id) {
        $service = Service::findOrFail($id);
        if(DB::table('salons_services')->where(['salonId' => $request->salonId, 'serviceId' => $id])->delete()) {
            return "Successfully deleted.";
        }
    }
}
