<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Review;
use App\Salon;
use App\Http\Resources\Review as ReviewResource;
use App\Http\Resources\ReviewCollection;

class ReviewController extends Controller
{
    public function getAll() {
        $reviews = Review::orderBy('created_at', 'asc')->get();
        foreach($reviews as $review) {
            $review->user = $review->user;
            $review->salon = $review->salon;
        }
        return $reviews;
    }

    public function getById($id) {
        $review = Review::findOrFail($id);
        $review->user = $review->user;
        $review->salon = $review->salon;
        return $review;
    }

    public function create(Request $request) {
        $userId = $request->input('userId');
        $salonId = $request->input('salonId');
        $reviewFound = Review::where(['salonId' => $salonId, 'userId' => $userId])->first();
        if ($reviewFound) {
            $this->update($request, $reviewFound->id);
        } else {
            $review = new Review;
            $review->title = $request->input('title');
            $review->description = $request->input('description');
            $review->rating = $request->input('rating');
            $review->salonId = $request->input('salonId');
            $review->userId = $request->input('userId');
            if($review->save()) {
                $salon = Salon::findOrFail($review->salonId);
                $newNoRatings = $salon->noRatings + 1;
                $newRating = ($salon->noRatings * $salon->rating + $review->rating) / $newNoRatings;
                $salon->rating = $newRating;
                $salon->noRatings = $newNoRatings;
                if ($salon->save()) {
                    return $review;
                }
            }
        }
    }

    public function update(Request $request, $id) {
        $review = Review::findOrFail($id);
        if ($request->input('title')) {
            $review->title = $request->input('title');
        }
        if ($request->input('description')) {
            $review->description = $request->input('description');
        }
        if ($request->input('rating')) {
            $oldRating = $review->rating;
            $review->rating = $request->input('rating');
            $salon = Salon::findOrFail($review->salonId);
            $newRating = ($salon->noRatings * $salon->rating - $oldRating + $review->rating) / $salon->noRatings;
            $salon->rating = $newRating;
            $salon->save();
        }
        if($review->save()) {
            return $review;
        } else return "test";
    }

    public function delete($id) {
        $review = Review::findOrFail($id);
        if ($review->delete()) {
            return "Successfully deleted.";
        }
        
    }
}
