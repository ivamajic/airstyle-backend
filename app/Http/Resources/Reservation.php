<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Reservation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'startTime' => $this->startTime,
            'endTime' => $this->endTime,
            'notes' => $this->notes,
            'salon' => $this->salon,
            'user' => $this->user,
            'employee' => $this->employee,
            'services' => $this->services
        ];
    }
}
