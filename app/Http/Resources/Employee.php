<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Salon;
use DB;

class Employee extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'image' => $this->image,
            'rating' => $this->rating,
            'noRatings' => $this->noRatings,
            'salon' => $this->salon
        ];
    }
}
