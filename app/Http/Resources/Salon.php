<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Salon extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'name' => $this->name,
            'image' => $this->image,
            'workHoursStart' => $this->workHoursStart,
            'workHoursEnd' => $this->workHoursEnd,
            'email' => $this->email,
            'address' => $this->address,
            'city' => $this->city,
            'country' => $this->country,
            'postCode' => $this->postCode,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'phone' => $this->phone,
            'rating' => $this->rating,
            'noRatings' => $this->noRatings,
            'employees' => $this->employees,
            'services' => $this->services
        ];
    }
}
