<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationService extends Model
{
    protected $fillable = [
        'reservationId',
        'salonId'
    ];

    protected $table = 'reservations_services';
}
