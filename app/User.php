<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstName',
        'lastName',
        'email',
        'password',
        'image',
        'role',
        'salonId'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /** Returns the salon the user is admin of. */
    public function salon() {
        return $this->belongsTo('App\Salon', 'salonId');
    }

    /** Returns all the reviews the user wrote. */
    public function reviews() {
        return $this->hasMany('App\Review', 'userId');
    }

    /** Returns all the reservations the user made. */
    public function reservations() {
        return $this->hasMany('App\Reservation', 'userId');
    }
}
